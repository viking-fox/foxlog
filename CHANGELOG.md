# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2019-12-13)


### Features

* **conf:** Add jest and ts-jest ([57a086e](https://gitlab.com/viking-fox/foxlog/commit/57a086e1b76a556e714e3c666513b93bc678e215))
* **console:** Add clear console helper ([ad7ea4a](https://gitlab.com/viking-fox/foxlog/commit/ad7ea4a6f247baca34f23e9ad07aa7130134bd32))
* **console:** Print stats ([f9b6f8c](https://gitlab.com/viking-fox/foxlog/commit/f9b6f8cd7c91c9341c86f870a64fe90554291146))
* **main:** Add main interval loop ([3a1d0b9](https://gitlab.com/viking-fox/foxlog/commit/3a1d0b9bd6a2926d69459f8a2e4c42e669c29ecf))
* **parser:** Add main class structure and types ([2f37f74](https://gitlab.com/viking-fox/foxlog/commit/2f37f74fb7abe3d3e72c3705ce51c04aefc6bdf6))
* **parser:** Change structure of parser ([b5f296e](https://gitlab.com/viking-fox/foxlog/commit/b5f296e05298e50d2f3885b160df32c1f28622a0))
* **parser:** Move parser bones from service to ([9e224e3](https://gitlab.com/viking-fox/foxlog/commit/9e224e3f770f414f808353b680a26b862ff5b7de))
* **parser:** Stricter section catch ([6172d05](https://gitlab.com/viking-fox/foxlog/commit/6172d05d1e72f8ae28af11204c4359ee0d2a37a6))
* **parser:** Update parser ([303980d](https://gitlab.com/viking-fox/foxlog/commit/303980d806bc99dac458c23f9217f921fcdb19d9))
* **parser:** Update parser with process ([7fc705a](https://gitlab.com/viking-fox/foxlog/commit/7fc705a0a2caf347c55a591613ce1eb4b012c05a))
* **parser:** Update service with stricter types ([fdbeada](https://gitlab.com/viking-fox/foxlog/commit/fdbeada626fda46d76b51c6da9fda87429ef9edb))
* **stats:** Add stats helper ([abce0b3](https://gitlab.com/viking-fox/foxlog/commit/abce0b3bbcef32c5e39fb369dd3e1e63d8cd0be4))
* **stats:** Update types and stats ([d437c9c](https://gitlab.com/viking-fox/foxlog/commit/d437c9c2dbe8801bb13994c135371c4a3958f9a1))
* Create main event class and main process ([fff0d1c](https://gitlab.com/viking-fox/foxlog/commit/fff0d1c29b35f33f7cd4ff4ff13b389c94f20838))
* Initial commit ([c383e8a](https://gitlab.com/viking-fox/foxlog/commit/c383e8a6bc87d673e72e784a276c971e348253ba))
* Update main process and events ([48813d3](https://gitlab.com/viking-fox/foxlog/commit/48813d376a6d0e7b2d75b9629dd06158277cea48))
