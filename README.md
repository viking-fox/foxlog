# FoxLog

## What is it ?

This is a console app to read an W3c fromatted HTTP access log
and print stats about thoses lines.

### Input

A section is defined as being what's before the second / in the log line's resource section. For
example, the section for /pages/create is /pages.
Here are some log line examples your program could receive:
```
#Fields: remotehost rfc931 authuser [date] "request" status bytes
127.0.01 - junji [09/May/2018 00:34:23 +0000] "GET /foo HTTP/1.0" 200 123
127.0.01 - uzumaki [09/May/2018 00:34:23 +0000] "GET /foo HTTP/1.0" 200 123
127.0.01 - vendetta [09/May/2018 00:34:23 +0000] "GET /foo HTTP/1.0" 200 123
127.0.01 - vendetta [09/May/2018 00:34:23 +0000] "GET /foo HTTP/1.0" 200 123
```

### Output

Program will print some statistics every `STATS_REFRESH` seconds.

For now the program will print the following statistics:

• Sections of the web site that are the most visited.

• Error rates by sections.

### Nice to have

- Stats as a whole (Bdd and service to get parsed ddata)
- Admin interface to show stats with graph
- Check if date is valid on each line ?

## Usage

Set log file through environment variable: `FOXLOG_FILE_PATH`;

Set refresh rates in second through environment variable: `FOXLOG_REFRESH_TICK`

- Install packages with : `npm i`;
- Build Project with : `npm run build`;
- Launch `npm run start` to run the program in production environment
- Launch `npm run dev` to run in a dev environment.
