import { EventEmitter } from 'events';
import debugCreate from 'debug';
const debug = debugCreate('foxlog-dev:error');

class MainEmitter extends EventEmitter {}

const mainEmitter =  new MainEmitter();

mainEmitter.on('error', (error: Error)  =>  {
    debug(`Error: ${error}`);
    process.exit(0);
})

export default mainEmitter;