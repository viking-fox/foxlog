import { EventEmitter } from 'events';

type AnyObj = { [key: string]: any };

// Main config
interface IConf {
    filePath: string,
    refreshTick: number,
    datas: IData[],
    emitter: EventEmitter,
    services: IConfServices | null
}

interface IConfServices {
}

// interface IServiceOptions {
//     filePath?: string,
//     refreshTick?: number,
//     datas?: IData[],
// }

enum EHttpMethod {
    'GET',
    'HEAD',
    'POST',
    'PUT',
    'DELETE',
    'CONNECT',
    'OPTIONS',
    'TRACE',
    'PATCH'
}
interface IData {
    host: string,
    authUser: string,
    date: string,
    httpMethod: EHttpMethod,
    section: string,
    status: number,
    bytes: number
}

interface IRegexLineGroups extends AnyObj{
    host: string,
    rfc931: string,
    authUser: string,
    date: string,
    req: string,
    status: string,
    bytes: string
}

interface ICountElement {
    name: string,
    count: number
}

export {
    IData,
    IConf,
    IConfServices,
    AnyObj,
    IRegexLineGroups,
    EHttpMethod,
    ICountElement
}