import createDebug from 'debug';
import * as Parser from './index';
import fs from 'fs';
import { promisify } from 'util';

// const
const wrongLog1Path = `${__dirname}/../../../files/log_wrong_format_1`;
const goodLog1Path = `${__dirname}/../../../files/log_good_format_1`;

const readFileAsync = promisify(fs.readFile);
const debug = createDebug('foxlog-test:parser');

describe('Parser Helper', ()   =>  {
    test('Service exist', done    =>  {
        expect(Parser).not.toBeUndefined();
        done();
    });

    describe('splitLines', ()   =>  {
        test('Throw error if no data or empty', done => {
            let err: any = null;
            try {
                // @ts-ignore
                Parser.splitLines();
            } catch(e) {
                err = e;
            }
            expect(typeof err).toBe('object');
            expect(err.message).toBe('No data received');
            done();
        });

        test('Return array of length of line(1)', done =>  {
            const line = '127.0.01 - vendetta [09/May/2018 00:34:23 +0000] "GET /cassos/foo HTTP/1.0" 200 123';
            const res = Parser.splitLines(line);
            expect(Array.isArray(res)).toBeTruthy();
            expect(res.length).toBe(1);
            done();
        });
    });

    describe('processLine', ()  =>  {
        test('Return null if no line', done =>  {
            // @ts-ignore
            const res = Parser.processLine();
            expect(res).toBeNull();
            done();
        });

        test('Return null if line is comment', done =>  {
            const res = Parser.processLine('#balbalecbzeicubze');
            expect(res).toBeNull();
            done();
        });

        test('Return null if wrongly formatted(Too much space)', done   =>  {
            const line = '127.0.01  - vendetta [09/May/2018 00:34:23 +0000] "GET /cassos/foo HTTP/1.0" 200 123';
            const res = Parser.processLine(line);
            expect(res).toBeNull();
            done()
        });

        test('Return null if wrongly formatted(bad http method)', done  =>  {
            const line = '127.0.01 - vendetta [09/May/2018 00:34:23 +0000] "GITGUD /cassos/foo HTTP/1.0" 200 123';
            const res = Parser.processLine(line);
            expect(res).toBeNull();
            done();
        });

        test('Return null if wrongly formatted(unknown status)', done =>    {
            const line = '127.0.01 - vendetta [09/May/2018 00:34:23 +0000] "GET /cassos/foo HTTP/1.0" 800 123';
            const res = Parser.processLine(line);
            expect(res).toBeNull();
            done();
        })
    });

    describe('exec', () =>  {
        test('Throw error if no data or empty string', async done =>  {
            let err: any = null;
            try {
                await Parser.execute(null);
            } catch(e) {
                err = e;
            }
            expect(typeof err).toBe('object');
            expect(err.message).toBe('No data received');
            done();
        });

        test('Return empty array if bad line format', async done   =>  {
            const str = await readFileAsync(wrongLog1Path, 'utf8');
            const lines = Parser.splitLines(str);
            const data = Parser.execute(lines);
            expect(Array.isArray(data)).toBeTruthy();
            expect(data.length).toBe(0);
            done(); 
        });

        test('Return data if good format', async done   =>  {
            debug(__dirname);
            const str = await readFileAsync(goodLog1Path, 'utf8');
            const lines = Parser.splitLines(str);
            const data = Parser.execute(lines);
            expect(Array.isArray(data)).toBeTruthy();
            expect(data.length).toBeGreaterThan(0);
            const line = data[0];
            
            expect(line.host).toBeTruthy();
            expect(line.host.length).toBeGreaterThan(0);

            expect(line.authUser).toBeTruthy();
            expect(line.authUser).toBeTruthy();
            expect(line.date).toBeTruthy();
            expect(line.httpMethod).toBeTruthy();
            expect(line.section).toBeTruthy();
            expect(typeof line.status).toBe('number')
            expect(typeof line.bytes).toBe('number');
            expect(line.status).toBeGreaterThan(100);
            expect(line.status).toBeLessThan(600);

            done();
        })
    })
})