// Package
import debugCreate from 'debug';

// Types
import { IData, IRegexLineGroups, EHttpMethod } from '../../types';

// Constants
const debug = debugCreate('foxlog-dev:parser');
const lineRegex = new RegExp(/^(?<host>\S+)\s(?<rfc931>\S+)\s(?<authUser>[^\s\[\]]+)\s\[(?<date>[^\]]+)\]\s"(?<req>[^"]+)"\s(?<status>\d+)\s(?<bytes>\d+)/);


const splitLines = (data: string):string[] => {
    if (!data || data.length === 0) {
        throw new Error('No data received');
    }

    return data.split(/\n/);
}

const execute = (lines: string[], offset?: number): IData[] => {

    debug(`data received:`, lines);
    const formattedData:IData[] =  [];
    
    if (!lines || lines.length === 0) {
        throw new Error('No data received');
    }

    if(!offset) {
        offset = 0;
    }
    
    // Start process
    // for so we can return
    for (const line of lines) {
        if (offset > 0) {
            offset--;
            continue;
        }
        const lineData = processLine(line);
        if (lineData) {
            formattedData.push(lineData);
        }
        debug('line data: ', lineData);
    }

    return formattedData;
}

const processLine = (line: string): null | IData => {
    if (!line || line[0] === '#') {
        return null;
    }
    debug('Line: ', line);
    // check if match regex
    const match =  line.match(lineRegex);
    
    if (!match) {
        return null;
    }

    const groups = match.groups as IRegexLineGroups;

    if (!groups || Object.keys(groups).length !== 7) {
        return null;
    }
    // Split Req string
    const splitted = groups.req.split(' ');

    if (splitted.length !== 3) {
        return null;
    }

    debug('Pass splitted');
    // Check if good http method
    let httpMethod:unknown = null;
    for(const code in EHttpMethod) {
        if (code === splitted[0]) {
            httpMethod = code;
            break;
        }
    }

    if (!httpMethod) {
        return null;
    }
    const http = httpMethod as EHttpMethod;

    debug('Pass method');
    // Check if good section
    const sectionSplit = splitted[1].split('/');
    debug('Section split: ', sectionSplit);
    if (!(sectionSplit.length >= 2)) {
        return null;
    }
    const matches = sectionSplit[1].match(/^(^[^?]+)/);
    debug('Matches: ', matches);
    const section = matches ? `/${matches[1]}` : `/${sectionSplit[1]}`;
    if (!section) {
        return null;
    }
    debug('Pass section');
    // Assign values
    const {host, authUser, date, status, bytes} = groups;

    if (!host || !date || !status){
        return null;
    }

    // Check if good http code
    if ((Number(status) < 100) || Number(status) > 599) {
        return null;
    }

    // Check if date is valid

    return {
        host,
        authUser,
        date,
        section,
        status: Number(status),
        bytes: Number(bytes),
        httpMethod: http,
    }
}

export  {
    execute,
    splitLines,
    processLine
};