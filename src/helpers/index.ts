import * as Parser from './parser';
import * as Stats from './stats';
import readline from 'readline';

const clearConsole = () =>  {
    const blank = '\n'.repeat(process.stdout.rows)
    console.log(blank)
    readline.cursorTo(process.stdout, 0, 0)
    readline.clearScreenDown(process.stdout)
};


export {
    Parser,
    Stats,
    clearConsole
}