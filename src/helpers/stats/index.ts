// Package
import createDebug from 'debug';

// Types 
import { IData, ICountElement } from '../../types';

// constant
const debug = createDebug('foxlog-dev:stats');

const sortIcountElements = (elements: ICountElement[]): ICountElement[] => {
    return elements.sort((a, b)   =>  {
        if (a.count === b.count) {
            return 0;
        }
        return a.count < b.count ?-1 : -1;
    });
}

const errorRatesBySection = (datas: IData[]): ICountElement[]   =>  {
    const count: ICountElement[] = [];

    debug('start error count');

    for(const data of datas) {
        // check if error
        const status = Number(data.status);
        if (status < 400) {
            continue;
        }
        // find in count
        let found = count.find(e => e.name === data.section)

        if (!found) {
            const el = {
                name: data.section,
                count: 0
            }
            count.push(el);
            found = el;
        }

        found.count++;
    }
    debug('end error count');
    return sortIcountElements(count);
};

const sortByMostVisited = (datas: IData[]): ICountElement[] => {
    debug('start most visited');
    const count: ICountElement[] = [];


    for(const data of datas) {
        // find in count
        let found = count.find(e  =>  e.name === data.section)

        if (!found) {
            const el = {
                name: data.section,
                count: 0
            };
            count.push(el);
            found = el;
        }
        found.count++
    }
    
    debug('end most visited');
    return sortIcountElements(count);
}

export { sortByMostVisited, errorRatesBySection};