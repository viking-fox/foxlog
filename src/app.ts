// package
import debugCreate from 'debug';
import emitter from './app.events';
import fs from 'fs';
import { promisify } from 'util';

// Helper
import { Parser, Stats, clearConsole }  from './helpers'
// Types
import { IConf } from './types';

const { existsSync } = fs;
const readFileAsync = promisify(fs.readFile);
const debug = debugCreate('foxlog-dev:main');

// Conf who will be used in the services
const conf: IConf = {
    filePath: process.env.FOXLOG_FILE_PATH || '/var/log/nginx/access.log',
    refreshTick: Number(process.env.FOXLOG_REFRESH_TICK) || 10,
    datas: [],
    emitter,
    services: null
};

const printLogs = () => {
    // copy data
    const statsData = [...conf.datas];
    // reset datas
    conf.datas = [];
    // get most visited
    const mostVisited = Stats.sortByMostVisited(statsData);
    debug('most visited: ', mostVisited);
    const mostError = Stats.errorRatesBySection(statsData);
    clearConsole();

    console.log('======= Most visited sections: \n');
    if (mostVisited.length === 0) {
        console.log('No traffic catched \n');
    } else {
        mostVisited.forEach(count   =>  {
            console.log(`Section: ${count.name} => ${count.count} times \n`);
        });
    }
    

    console.log('======= Most Error by section: \n');
    if (mostError.length === 0) {
        console.log('No error catched \n');
    } else {
        mostError.forEach(count =>  {
            console.log(`Section: ${count.name} => ${count.count} error`)
        });
    }
    
    
}

const mainLoop = async () => {
    // Read content for the first time 
    let data = null;
    let lines = null;

    try {
        data = await readFileAsync(conf.filePath, 'utf8');
        lines = Parser.splitLines(data);
    } catch(err) {
        debug(err);
        emitter.emit('error');
    }
    let lineCount = lines.length;
    const formatedDatas = Parser.execute(lines);
    conf.datas = formatedDatas;

    debug('Init Number: ', conf.datas.length);
    printLogs();


    // Set event listener on file
    fs.watch(conf.filePath, {}, async (eventType: string, filename: string)  => {
        // check if file changed
        if (eventType !== 'change') {
            return;
        }
        const newDatas = await readFileAsync(conf.filePath, 'utf8');
        const newLines = Parser.splitLines(newDatas);
        debug('Old Count: ', lineCount)
        debug('NewCount: ', newLines.length);
        if (lineCount >= newLines.length) {
            return;
        }
        // Call parser with offset
        const newFormattedDatas = Parser.execute(newLines, lineCount);
        lineCount = newLines.length;
        conf.datas = [...conf.datas, ...newFormattedDatas];

        debug('Updated NUmber: ', conf.datas.length);
        debug(conf.datas);
    });

    // set interval
    setInterval(()  =>  printLogs(), 1000 * conf.refreshTick);
};

const main = async () => {
    console.log('App Started');
    // Check path of file
    try {
        if (conf.filePath.length === 0 || !existsSync(conf.filePath)) {
            throw new Error('File submitted does not exist');
        }
    } catch (err) {
        debug(err);
        emitter.emit('error', err);
    }

    // Check
    mainLoop();
};

main();
